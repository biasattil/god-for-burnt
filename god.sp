#include <sourcemod>
#include <cstrike>
#include <sdktools>
#include <sdkhooks>
#include <clientprefs>

#pragma semicolon 1
#pragma newdecls required

bool g_bGodSlayHappened[MAXPLAYERS + 1];
Handle g_hGodCookie = INVALID_HANDLE;

public Plugin myinfo =
{
	name = "God Plugin",
	author = "Sit.",
	description = "if u know u know",
	version = "1.0.0",
	url = "Hellsgamers.com"
};

//Initialize cookie and command
public void OnPluginStart() 
{
	RegConsoleCmd("sm_god", Command_God, "Turns the user into a god among men");
	g_hGodCookie = RegClientCookie("cookie_godmode", "God mode you feel", CookieAccess_Public);
}

//Check if user has g_hGodCookie at 0 || 1, and set it to 0
//If cookie is set kill all if g_hGodCookie == 1 when user enters server
public void OnClientPutInServer(int client)
{
	char sCookieValue[12];
	GetClientCookie(client, g_hGodCookie, sCookieValue, sizeof(sCookieValue));
	
	if(sCookieValue[0] != '1' && sCookieValue[0] != '0') 
	{	
		SetClientCookie(client, g_hGodCookie, "0");
	} 
	else if(sCookieValue[0] == '1') 
	{	
		Command_KillAll(client, 0);
	}
}

//Activate menu for user
public Action Command_God(int client, int args)
{
	God_Menu(client);	
	return Plugin_Handled;
}

//Menu creation
public void God_Menu(int client)
{
	Menu menu = CreateMenu(Callback_GodMenu);	
	menu.SetTitle("God Mode Options");
	
	char sCookieValue[12];
	GetClientCookie(client, g_hGodCookie, sCookieValue, sizeof(sCookieValue));
	
	if(sCookieValue[0] == '0')
	{	
		menu.AddItem("enableOption", "Enable God Mode");
	} 
	else {
		menu.AddItem("disableOption", "Disable God Mode");
	}
	
	menu.AddItem("killOption", "Kill All Players");
	menu.Display(client, 0);
}

//Menu logic
public int Callback_GodMenu(Menu menu, MenuAction action, int client, int itemNum)
{
	switch(action)
	{
		case MenuAction_Select:
		{
			char info[32];
			menu.GetItem(itemNum, info, sizeof(info));

			if(StrEqual(info, "enableOption"))
			{	
				Command_GodOn(client, 0);
				God_Menu(client);
			}
			else if(StrEqual(info, "disableOption"))
			{ 	
				Command_GodOff(client, 0);
			 	God_Menu(client);
			} 
			else if(StrEqual(info, "killOption"))
			{ 	
				Command_KillAll(client, 0);
			 	God_Menu(client);
			}
			else
			{	
				God_Menu(client);			
			}
		}
		case MenuAction_End:
		{
			delete menu;
		}
	}
}

//Turn god mode on and SDKHook the client for damage nullification
public Action Command_GodOn(int client, int args)
{
	if(!IsClientValid(client))
	{
		return Plugin_Handled;
	}
	SetClientCookie(client, g_hGodCookie, "1");
	
	if(!g_bGodSlayHappened[client])
	{	
		Command_KillAll(client, 0);
		g_bGodSlayHappened[client] = true;
	}
	SDKHook(client, SDKHook_OnTakeDamage, OnTakeDamage);

	return Plugin_Handled;
}

//Turn god mode off and unhook the client
public Action Command_GodOff(int client, int args)
{
	if(!IsClientValid(client))
	{
		return Plugin_Handled;
	}
	SetClientCookie(client, g_hGodCookie, "0");
	SDKUnhook(client, SDKHook_OnTakeDamage, OnTakeDamage);

	return Plugin_Handled;
}

//Loop through players and force suicide if they aren't the person who activated godmode
public Action Command_KillAll(int client, int args)
{
	for(int i = 1; i <= MaxClients; i++)
	{	
		if(i != client && IsClientValid(i))
		{	
			ForcePlayerSuicide(i);
		}
	}
	PrintToChatAll("Later Retards");
	
	return Plugin_Handled;
}

//If client is SDKHooked, they won't take damage (If other SDKHooks were being used in plugin, you'd need to check g_hGodCookie)
public Action OnTakeDamage(int victim, int &attacker, int &inflictor, float &damage, int &damagetype, int &weapon, float damageForce[3], float damagePosition[3], int damagecustom)
{
	damage = 0.0;	
	return Plugin_Changed;
}

//Check if client is actually playing the game
public bool IsClientValid(int client)
{
	if(client <= 0 || client > MaxClients || !IsClientConnected(client) || !IsClientInGame(client) || IsFakeClient(client))
	{
		return false;
	}
	return true;
}